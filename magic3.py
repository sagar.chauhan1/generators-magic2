from magic2 import BankAccount

'''
Custom Iterator
Define a class Bank, which contains BankAccounts
class Bank:
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)
Use a magic method so that you can iterate through the accounts in the bank using the below syntax
bank = Bank()

for account in bank:
    print(account)'''

class Bank(BankAccount):
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)

    def __iter__(self):
        self.id = 0
        return self

    def __next__(self):
        x = self.id
        if self.id == len(self.accounts):
            raise StopIteration
        self.id += 1
        return bank.accounts[i]

if __name__ == '__main__':
    bank = Bank()
    itr = iter(bank)
    for i in range(100):
        bank.add_account(BankAccount(id=i, balance=i * 100))
    for account in iter(bank):
        print(account)
