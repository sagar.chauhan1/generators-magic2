'''
Define a zip function that works similar to the builtin zip function. (Unlike the builtin zip, assume that all iterables are of the same size)

def gen_zip(*iterables):
    pass'''

def gen_zip(*iterables):
    #print(iterables)
    for values in iterables:
        for value in values:
            yield value[0], value[1]

l = [(0,1),(2,3),(4,5),(6,7),(8,9)]
list1 = []
list2 = []
for i,j in gen_zip(l):
    list1.append(i)
    list2.append(j)
print(list1)
print(list2)
