#Warmup

#Add two integers using a suitable magic method
a = 5
b = 6
print(a.__add__(b))
#Find the length of a list using a suitable magic method
l = [1,2,3,4,5,6,7]
print(l.__len__())
#Find the length of a string using a suitable magic method
s = "HELLO WORLD"
print(s.__len__())
#Add two lists by calling a magic method on the first list
l1 = [1,2,3,4]
l2 = [5,6,7,8]
print(l1.__add__(l2))
'''
Sortable and comparable classes:
i) Create a class, BankAccount as below
class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)
ii) Create a list of BankAccounts
import random
accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(100)]
iii) Sort accounts by balance, using suitable magic methods
iv) Compare whether two bank accounts are equal. Two bank accounts are equal if they have the same id and balance
a1 = BankAccount(id=1, balance=100)
a2 = BankAccount(id=1, balance=100)
a3 = BankAccount(id=2, balance=100)

a1 == a2 # True
a1 == a3 # False'''

import random
class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance
    def __eq__(self, other):
        if self.id == other.id and self.balance == other.balance:
            return True
        else:
            return False

    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)

if __name__ == '__main__':
    accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(100)]
    for i in range(accounts.__len__()):
        for j in range(i,accounts.__len__()):
            if accounts[i].balance.__gt__(accounts[j].balance):
                accounts[i], accounts[j] = accounts[j], accounts[i]
    print(accounts[23].__eq__(accounts[54]))

'''
Custom Iterator
Define a class Bank, which contains BankAccounts
class Bank:
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)
Use a magic method so that you can iterate through the accounts in the bank using the below syntax
bank = Bank()

for account in bank:
    print(account)'''

class Bank(BankAccount):
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)

    def __iter__(self):
        self.id = 0
        return self

    def __next__(self):
        x = self.id
        if self.id == len(self.accounts):
            raise StopIteration
        self.id += 1
        return bank.accounts[i]

if __name__ == '__main__':
    bank = Bank()
    itr = iter(bank)
    for i in range(100):
        bank.add_account(BankAccount(id=i, balance=i * 100))
    for account in iter(bank):
        print(account)
'''
Define a gen_range function that works like the builtin range function. It should yield a number in each iteration

def gen_range(start, stop, step):
    pass
'''
def gen_range(start,stop,step):
    while start < stop:
        yield start
        start += step

gen_obj = gen_range(2,100,2)
for value in gen_obj:
    print(value)
'''
Define a zip function that works similar to the builtin zip function. (Unlike the builtin zip, assume that all iterables are of the same size)

def gen_zip(*iterables):
    pass'''

def gen_zip(*iterables):
    #print(iterables)
    for list in iterables:
        for set in list:
            yield set[0], set[1]

l = [(0,1),(2,3),(4,5),(6,7),(8,9)]
list1 = []
list2 = []
for i,j in gen_zip(l):
    list1.append(i)
    list2.append(j)
print(list1)
print(list2)
