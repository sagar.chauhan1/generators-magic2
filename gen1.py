'''
Define a gen_range function that works like the builtin range function. It should yield a number in each iteration

def gen_range(start, stop, step):
    pass
'''


def gen_range(start, stop, step=1):
	if step > 0:
	    while start < stop:
        	yield start
        	start += step
	elif step < 0:
	    while start > stop:
        	yield start
        	start += step

gen_obj = gen_range(2,100,2)
for value in gen_obj:
    print(value)

gen_obj = gen_range(100,0,-2)
for value in gen_obj:
    print(value)
