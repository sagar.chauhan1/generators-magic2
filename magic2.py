'''
Sortable and comparable classes:
i) Create a class, BankAccount as below
class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)
ii) Create a list of BankAccounts
import random
accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(100)]
iii) Sort accounts by balance, using suitable magic methods
iv) Compare whether two bank accounts are equal. Two bank accounts are equal if they have the same id and balance
a1 = BankAccount(id=1, balance=100)
a2 = BankAccount(id=1, balance=100)
a3 = BankAccount(id=2, balance=100)

a1 == a2 # True
a1 == a3 # False'''

import random
class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __gt__(self,other):
        if self > other:
            return True
        else:
            return False
            
    def __eq__(self, other):
        if self.id == other.id and self.balance == other.balance:
            return True
        else:
            return False

    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)

if __name__ == '__main__':
    accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(100)]
    for i in range(accounts.__len__()):
        for j in range(i,accounts.__len__()):
            if accounts[i].balance.__gt__(accounts[j].balance):
                accounts[i], accounts[j] = accounts[j], accounts[i]
    print(accounts[23].__eq__(accounts[54]))
