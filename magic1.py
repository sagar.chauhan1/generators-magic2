#Warmup

#Add two integers using a suitable magic method
a = 5
b = 6
print(a.__add__(b))
#Find the length of a list using a suitable magic method
l = [1,2,3,4,5,6,7]
print(l.__len__())
#Find the length of a string using a suitable magic method
s = "HELLO WORLD"
print(s.__len__())
#Add two lists by calling a magic method on the first list
l1 = [1,2,3,4]
l2 = [5,6,7,8]
print(l1.__add__(l2))